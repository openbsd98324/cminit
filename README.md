# cminit

a little custom init


## Compilation

````
  cc -static  src/init.c  -o /bin/cminit 
````


## GRUB 

 Using alike init=/bin/bash or this config: init=/bin/cminit   

````
menuentry 'Linux Devuan X11, WM on sda2 (tm, stable)' --class devuan --class gnu-linux --class gnu { 
	insmod gzio
	insmod part_msdos
	insmod ext2
	set root='hd0,msdos2'
	linux	/boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda2  rw   init=/bin/cminit   
	initrd	/boot/initrd.img-4.9.0-11-amd64
}
````



 

