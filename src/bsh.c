

//
// barebone command interpreter 
// RUNS DIRECTLY CALLED, FROM /sbin/init 
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <time.h>
#define PAMAX 2046 





void nls()
{ 
	DIR *dirp;
	struct dirent *dp;
	dirp = opendir( "." );
	while  ((dp = readdir( dirp )) != NULL ) 
	{
		if (  strcmp( dp->d_name, "." ) != 0 )
			if (  strcmp( dp->d_name, ".." ) != 0 )
				printf( "%s\n", dp->d_name );
	}
	closedir( dirp );
}





static int cat_fd(int fd) 
{
  char buf[4096];
  ssize_t nread;

  while ((nread = read(fd, buf, sizeof buf)) > 0) 
  {
    ssize_t ntotalwritten = 0;
    while ( ntotalwritten < nread) 
    {

      ssize_t nwritten = write( STDOUT_FILENO, buf + ntotalwritten, nread - ntotalwritten);

      if ( nwritten < 1 )
        return -1;

      ntotalwritten += nwritten;
    }
  }
  return nread == 0 ? 0 : -1;
}






static int minicat(const char *fname) 
{
  int fd, success;
  if ((fd = open(fname, O_RDONLY)) == -1)
    return -1;

  success = cat_fd(fd);

  if (close(fd) != 0)
    return -1;

  return success;
}








//#include <unistd.h>
//pid_t fork(void);

// system is a call that is made up of 3 other system calls: execl(), wait()
// and fork() (which are prototyed in <unistd>)

// // 
//  http://users.cs.cf.ac.uk/Dave.Marshall/CM2204/PDF/C.pdf
//

//int main()
int main( int argc, char *argv[])
{
        pid_t pid = 0; int status;


        // a little busybox alike.
	if ( argc >= 2 )
	if ( ( strcmp( argv[1] , "nls" ) ==  0 ) || ( strcmp( argv[1] , "-nls" ) ==  0 ) || ( strcmp( argv[1] , "--nls" ) ==  0 ) ) 
	{
			if ( argc == 3 )
			{
				chdir( argv[ 2 ] );
				nls();
			}
			else
				nls( ); 
			return 0;
	}
	//
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "-nls" ) ==  0 ) || ( strcmp( argv[1] , "nls" ) ==  0 ) )
	{
		//nls( "." );
		nls(  );
		return 0;
	}





	printf( " == == === \n" ); 
	printf( " == BSH == \n" ); 
	printf( " == == === \n" ); 
	printf( "\n" ); 
	printf( " !help: help\n" ); 
	printf( "\n" ); 





	char var_lastcommand[PAMAX];
	char target[PAMAX];
	char foocharo[PAMAX];

	fprintf( stderr, "Welcome to BSH, Linux Barebone\n" ); 
	fprintf( stderr, " (Esc, 27) to quit.\n" ); 
	char commandprompt[PAMAX];
	char charo[PAMAX];
	char cwd[PAMAX];
	strncpy( commandprompt, "", PAMAX );

	struct termios ot;
	if(tcgetattr(STDIN_FILENO, &ot) == -1) perror(")-");
	struct termios t = ot;
	t.c_lflag &= ~(ECHO | ICANON);
	t.c_cc[VMIN] = 1;
	t.c_cc[VTIME] = 0;
	if(tcsetattr(STDIN_FILENO, TCSANOW, &t) == -1) perror(")-");

	int a = 0;
	int gameover = 0; 
	int cmdlen = 0; 
	int debug = 0; 
	while( gameover == 0 ) 
	{
		//fprintf( stderr, "you pressed '%c' (d %d)\n", a, a );
		fprintf( stderr, "A:> %s\n", commandprompt ); 
		a = getchar(); 
		if ( a == 27 ) gameover = 1 ; 

		else if  ( a == 127 )
		{
		       strncpy( commandprompt, "" , PAMAX);
		}

		else if ( ( a == 13 ) || ( a == 10 ) )
		{
		        if ( debug == 1 )
			{
		           printf( "Char  %d \n" , a );   // 10 is enter  line  1 
		           //printf( "Strln %d \n" , strlen( commandprompt ) );   // 10 is enter  line 2 
			   // int , but it has unsigneed long -Wformat 
			   // so %lu 
		           printf( "Strln %lu \n" , strlen( commandprompt ) );   // 10 is enter  line 2 
			}

			cmdlen = strlen( commandprompt );

			if ( ( commandprompt[0]    == 'c'  ) 
			  && ( commandprompt[1]  == 'd'  ) 
			  && ( commandprompt[2]  == ' '  ) ) 
			{
				strcpy( target, commandprompt +3 );
				fprintf( stderr, "Strip %s\n", target ); 
				chdir(  target ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}




			else if ( ( commandprompt[0]           == '!'  )
					&& ( commandprompt[1]  == 'q'  )   
					&& ( commandprompt[2]  == 'u'  )
					&& ( commandprompt[3]  == 'i'  )   
					&& ( commandprompt[4]  == 't'  )
					&& ( cmdlen == 4+1 ) ) 
			{
				gameover = 1; 
				strncpy( commandprompt, "" , PAMAX);
			}


			else if ( ( commandprompt[0]    == 'n'  )
					&& ( commandprompt[1]  == 'l'  )   
					&& ( commandprompt[2]  == 's'  )
					&& ( cmdlen == 3 ) ) 
			{
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				nls(); 
				strncpy( commandprompt, "" , PAMAX);
			}




			else if ( ( commandprompt[0]    == 'p'  )
			 && ( commandprompt[1]  == 'w'  )   
			 && ( commandprompt[2]  == 'd'  )
			 && ( cmdlen == 3 ) ) 
			{
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}





			else if ( ( commandprompt[0]    == '!'  )
			 && ( commandprompt[1]  == 'h'  )   
			 && ( commandprompt[2]  == 'e'  )   
			 && ( commandprompt[3]  == 'l'  )   
			 && ( commandprompt[4]  == 'p'  )   
			 && ( cmdlen == 4+1 ) ) 
			{
				fprintf( stderr, "==================\n" ); 
				fprintf( stderr, "=== soft: BSH  ===\n" ); 
				fprintf( stderr, "==================\n" ); 
				fprintf( stderr, "=== !HELP ===\n" ); 
				fprintf( stderr, "\n" ); 
				fprintf( stderr, "pwd:   shows the current path.\n" ); 
				fprintf( stderr, "cd:    change current directory.\n" ); 
				fprintf( stderr, "\n" ); 
				fprintf( stderr, "!last: shows the last command.\n" ); 
				fprintf( stderr, "!time: gives the unix epoch time.\n" ); 
				fprintf( stderr, "\n" ); 
				fprintf( stderr, "cmd:  cmd ls   (will run with -1)      \n" ); 
				fprintf( stderr, "sys:  sys bash (will run with --debug) \n" ); 
				fprintf( stderr, "\n" ); 
				fprintf( stderr, " !help: this help\n" ); 
				fprintf( stderr, " !quit: force quit\n" ); 
				fprintf( stderr, "\n" ); 
				strncpy( commandprompt, "" , PAMAX);
			}








			else if ( ( commandprompt[0]    == '!'  )
			 && ( commandprompt[1]  == 't'  )   
			 && ( commandprompt[2]  == 'i'  )   
			 && ( commandprompt[3]  == 'm'  )   
			 && ( commandprompt[4]  == 'e'  )   
			 && ( cmdlen == 4+1 ) ) 
			{
		                fprintf( stderr , "%d\n", (int)time(NULL));
				strncpy( commandprompt, "" , PAMAX);
			}





			else if ( ( commandprompt[0]    == '!'  )
			 && ( commandprompt[1]  == 'l'  )   
			 && ( commandprompt[2]  == 'a'  )   
			 && ( commandprompt[3]  == 's'  )   
			 && ( commandprompt[4]  == 't'  )   
			 && ( cmdlen == 4+1 ) ) 
			{
				printf( "===\n" );
				fprintf( stderr, "Last command:\n" ); 
				fprintf( stderr, "  %s\n", var_lastcommand ); 
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}



			else if ( ( commandprompt[0]    == 'c'  ) && ( commandprompt[1]  == 'd'  )   
			 && ( cmdlen == 1+1 ) ) 
			{
			        printf( "HOME: %s\n", getenv( "HOME" ));
				chdir(  getenv( "HOME" ) ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}





			else if ( ( commandprompt[0]    == 'c'  ) 
					&& ( commandprompt[1]  == 'a'  )   
					&& ( commandprompt[2]  == 't'  )   
					&& ( commandprompt[3]  == ' '  )  ) 
			{
				strcpy( target, commandprompt +4 );
				fprintf( stderr, "Strip %s\n", target ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
	                        snprintf( foocharo , sizeof( foocharo ), "%s" , target ); 
				printf( "CMD: %s\n" , foocharo ); 
                                minicat( foocharo ); 
				strncpy( commandprompt, "" , PAMAX);
			}




			/// HERE IT STOPS AFTER execl !! 
			///// FIXED WITH FORK !
			else if ( ( commandprompt[0]    == 'l'  ) && ( commandprompt[1]  == 's'  )   
			             && ( cmdlen == 1+1 ) ) 
			{
				pid = fork();
				if (pid == 0) {
					printf("I am the child.\n");
        			        int ret = execl( "/sbin/ls" , "ls", "-1", (char *)0);
					printf("I am the child, 10 seconds later.\n");
				}
				if (pid > 0) {
					printf("I am the parent, the child is %d.\n", pid);
					pid = wait(&status);
					printf("End of process %d: ", pid);
					if (WIFEXITED(status)) 
					{
						printf("The process ended with exit(%d).\n", WEXITSTATUS(status));
					}
					if (WIFSIGNALED(status)) 
					{
						printf("The process ended with kill -%d.\n", WTERMSIG(status));
					}
				}
				strncpy( commandprompt, "" , PAMAX);
			}




			else if ( ( commandprompt[0]           == 's'  ) 
					&& ( commandprompt[1]  == 'y'  )   
					&& ( commandprompt[2]  == 's'  )   
					&& ( commandprompt[3]  == ' '  )  ) 
			{
				strcpy( target, commandprompt +4 );
				fprintf( stderr, "Strip %s\n", target ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
	                        snprintf( foocharo , sizeof( foocharo ), "%s" , target ); 
				printf( "CMD: %s\n" , foocharo ); 

				pid = fork();
				if (pid == 0) {
					printf("I am the child.\n");
        			        int ret = execl( foocharo , foocharo , "--debug", (char *)0);
					printf("I am the child, 10 seconds later.\n");
				}
				if (pid > 0) {
					printf("I am the parent, the child is %d.\n", pid);
					pid = wait(&status);
					printf("End of process %d: ", pid);
					if (WIFEXITED(status)) 
					{
						printf("The process ended with exit(%d).\n", WEXITSTATUS(status));
					}
					if (WIFSIGNALED(status)) 
					{
						printf("The process ended with kill -%d.\n", WTERMSIG(status));
					}
				}
				strncpy( commandprompt, "" , PAMAX);
			}




			else if ( ( commandprompt[0]           == 'c'  ) 
					&& ( commandprompt[1]  == 'm'  )   
					&& ( commandprompt[2]  == 'd'  )   
					&& ( commandprompt[3]  == ' '  )  ) 
			{
				strcpy( target, commandprompt +4 );
				fprintf( stderr, "Strip %s\n", target ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
	                        snprintf( foocharo , sizeof( foocharo ), "%s" , target ); 
				printf( "CMD: %s\n" , foocharo ); 

				pid = fork();
				if (pid == 0) {
					printf("I am the child.\n");
        			        int ret = execl( foocharo , foocharo , "-1", (char *)0);
					printf("I am the child, 10 seconds later.\n");
				}
				if (pid > 0) {
					printf("I am the parent, the child is %d.\n", pid);
					pid = wait(&status);
					printf("End of process %d: ", pid);
					if (WIFEXITED(status)) 
					{
						printf("The process ended with exit(%d).\n", WEXITSTATUS(status));
					}
					if (WIFSIGNALED(status)) 
					{
						printf("The process ended with kill -%d.\n", WTERMSIG(status));
					}
				}
				strncpy( commandprompt, "" , PAMAX);
			}






			else
			{
				fprintf( stderr, "Run\n" ); 
	                        strncpy( var_lastcommand, commandprompt , PAMAX);
				system( commandprompt );
				// cleanup
				strncpy( commandprompt, "" , PAMAX);
			}
		}

		else
		{
			snprintf( charo, sizeof(charo), "%s%c", commandprompt, a );
			strncpy( commandprompt, charo , PAMAX);
		}
	}

	if(tcsetattr(STDIN_FILENO, TCSANOW, &ot) == -1) perror(")-");

	return 0; 
}






